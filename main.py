from flask import Flask
import subprocess
import os

app = Flask(__name__)

PORT = os.environ.get('GITPULLER_PORT', 8000)


@app.route("/", methods=['POST', 'GET'])
def gitpull():
    process = subprocess.Popen(['git', 'pull'],
                               cwd=os.environ.get('GITPULLER_DIR'),
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               )
    result = process.stdout.read() + process.stderr.read()
    return result


if __name__ == '__main__':
    app.run(port=PORT)
